# self study

    CS162: Operating Systems and Systems Programming
    
    [CS162 youtube list](https://www.youtube.com/playlist?list=PL-XXv-cvA_iBDyz-ba4yDskqMDY6A1w_c)
    [CS162 schedule](https://inst.eecs.berkeley.edu/~cs162/sp15/)

# Git

    [Ry’s Git Tutorial](http://rypress.com/tutorials/git/index)
    [Git from the inside out](https://codewords.recurse.com/issues/two/git-from-the-inside-out)
    [giteveryday(7) Manual Page ](https://www.kernel.org/pub/software/scm/git/docs/giteveryday.html)


# Deep learning

    [Deep Learning Isn’t a Dangerous Magic Genie. It’s Just Math](https://www.wired.com/2016/06/deep-learning-isnt-dangerous-magic-genie-just-math/)

    [Deep Learning 101](http://markus.com/deep-learning-101/)

    [neural-enhance](https://github.com/alexjc/neural-enhance) enhance image
    
    Nvidia - [Deep Learning in a Nutshell: Core Concepts](https://devblogs.nvidia.com/parallelforall/deep-learning-nutshell-core-concepts/)


    ## Convolution
    
        [Understanding Convolution in Deep Learning](http://timdettmers.com/2015/03/26/convolution-deep-learning/)
     
    ## Reinforcement
    
        [Guest Post (Part I): Demystifying Deep Reinforcement Learning](https://www.nervanasys.com/demystifying-deep-reinforcement-learning/)
        
    ## Tensorflow
    
        - [Hello tensorflow](https://www.oreilly.com/learning/hello-tensorflow)
        - [tensorflow doc](https://www.tensorflow.org/versions/r0.10/tutorials/mnist/beginners/index.html#mnist-for-ml-beginners)

[Applying the Linus Torvalds “Good Taste” Coding Requirement](https://medium.com/@bartobri/applying-the-linus-tarvolds-good-taste-coding-requirement-99749f37684a#.dgkuillco)

[I don't understand Python's Asyncio](http://lucumr.pocoo.org/2016/10/30/i-dont-understand-asyncio/)

[The Government IT Self-Harm Playbook](https://medium.com/@sheldonline/the-government-it-self-harm-playbook-6537d3920f65#.hqn510kln)

[unmaintainable-code](https://github.com/Droogans/unmaintainable-code)

[How to be a Programmer 中文版](https://braydie.gitbooks.io/how-to-be-a-programmer/content/zh/)

[造就優良的軟體開發團隊文化的要素有哪些？](https://softnshare.wordpress.com/2016/08/29/goodengineeringculture/)

[Google's "Director of Engineering" Hiring Test](http://www.gwan.com/blog/20160405.html)

[What is the most impressive (in a good or bad way) Python code you've seen on Github?](https://www.reddit.com/r/Python/comments/5aonm5/what_is_the_most_impressive_in_a_good_or_bad_way/)

