#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Lee'
SITENAME = 'Blog'
SITEURL = ''
# SITEURL = 'localhost:8080'


# THEME_STATIC_DIR = 'themes'
THEME = "themes/pelican-bootstrap3"
SEARCH_BOX = True
DISPLAY_CATEGORIES_ON_MENU = True
DISPLAY_PAGES_ON_MENU = True

RELATIVE_URLS = False
# SITESEARCH = '/search'

PATH = 'content'

TIMEZONE = 'Asia/Taipei'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

USE_FOLDER_AS_CATEGORY = True
# DEFAULT_CATEGORY = 'misc'

# default datetime is file time stamp
DEFAULT_DATE = 'fs'

PATH = 'content/'
OUTPUT_PATH = 'public'
# THEME_STATIC_DIR = 'theme/'
# THEME = './themes/photowall'
LOAD_CONTENT_CACHE = False
DELETE_OUTPUT_DIRECTORY = True

PLUGIN_PATHS = ['plugins']
PLUGINS = ['assets', 'ipynb.liquid', 'tipue_search', 'liquid_tags', 'tag_cloud']
PLUGINS += ['liquid_tags.img', 'liquid_tags.video',
            'liquid_tags.youtube', 'liquid_tags.vimeo',
            'liquid_tags.include_code']

DIRECT_TEMPLATES = (('index', 'tags', 'categories', 'authors', 'archives', 'search'))

STATIC_PATHS = ['imgs', 'downloads', 'notebooks']

# NOTEBOOK_DIR = 'notebooks'

MARKUP = ('md',)
